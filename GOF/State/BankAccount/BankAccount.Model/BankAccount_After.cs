using System.ComponentModel.Design;
using System.Diagnostics.Contracts;

namespace BankAccount.Model
{
    internal class AccountContext
    {
        public int Id { get; set; }
        public decimal Balance { get; set; }
    }

    public class BankAccount
    {
        internal static IAccountState NORMAL = new NormalState();
        internal static IAccountState OVERDRAFT = new OverdraftState();

        private AccountContext AccountContext { get; set; }

        private IAccountState _state;

        public int Id { get; private set; }

        public decimal Balance
        {
            get { return AccountContext.Balance; }            
        }

        public AccountState Status { get { return _state.State; } }

        public BankAccount(int id, decimal balance = 0.0M)
        {
            AccountContext = new AccountContext() { Id = id, Balance = balance};
    
            if (AccountContext.Balance >= 0)
                _state = BankAccount.NORMAL;
            else
            {
                _state = BankAccount.OVERDRAFT;
            }
        }

        public void Deposit(decimal amount)
        {
            Contract.Assert(amount > 0.0M);

            _state = _state.Deposit(amount, AccountContext);
        }

        public void Withdraw(decimal amount)
        {
            Contract.Assert(amount > 0.0M);

            _state = _state.Withdraw(amount, AccountContext);
        }

        public void PayInterest()
        {
            _state.PayInterest(AccountContext);
        }

        internal interface IAccountState
        {
            IAccountState Withdraw(decimal amount, AccountContext context);
            IAccountState PayInterest(AccountContext context);
            IAccountState Deposit(decimal amount, AccountContext context);
            AccountState State { get; }
        }

        class NormalState : IAccountState
        {
            public IAccountState Withdraw(decimal amount, AccountContext context)
            {
                context.Balance -= amount;

                if (context.Balance < 0)
                    return BankAccount.OVERDRAFT;
                return this;
            }

            public IAccountState PayInterest(AccountContext context)
            {
                context.Balance *= 1.1M;

                return this;
            }

            public IAccountState Deposit(decimal amount, AccountContext context)
            {
                context.Balance += amount;

                return this;
            }

            public AccountState State
            {
                get { return AccountState.Normal; }
            }
        }

        class OverdraftState : IAccountState
        {
            public IAccountState Withdraw(decimal amount, AccountContext context)
            {
                throw new InsufficientFunds(string.Format("Insufficient funds on account #1{0}", context.Id));
            }

            public IAccountState PayInterest(AccountContext context)
            {
                context.Balance *= 1.2M;

                return this;
            }

            public IAccountState Deposit(decimal amount, AccountContext context)
            {
                context.Balance += amount;

                if (context.Balance >= 0M)
                    return BankAccount.NORMAL;
                return this;
            }

            public AccountState State
            {
                get { return AccountState.Overdraft; }
            }
        }
    }
}