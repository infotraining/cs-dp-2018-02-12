using System.Diagnostics.Contracts;

namespace BankAccount.Model
{
    interface IAccountState
    {
        void Withdraw(decimal amount, BankAccount account);
        void Deposit(decimal amount, BankAccount account);
        void PayInterest(BankAccount account);
        AccountState Status { get; }
    }

    class NormalState : IAccountState
    {
        public void Withdraw(decimal amount, BankAccount account)
        {
            account.Balance -= amount;
        }

        public void Deposit(decimal amount, BankAccount account)
        {
            account.Balance += amount;
        }

        public void PayInterest(BankAccount account)
        {
            account.Balance *= 1.1M;
        }

        public AccountState Status
        {
            get { return AccountState.Normal; }
        }
    }

    class OverdraftState : IAccountState
    {
        public void Withdraw(decimal amount, BankAccount account)
        {
            throw new InsufficientFunds(string.Format("Insufficient funds on account #1{0}", account.Id));
        }

        public void Deposit(decimal amount, BankAccount account)
        {
            account.Balance += amount;
        }

        public void PayInterest(BankAccount account)
        {
            account.Balance *= 1.2M;
        }

        public AccountState Status
        {
            get { return AccountState.Overdraft; }
        }
    }

    public class BankAccount
    {
        public int Id { get; private set; }
        public decimal Balance { get; internal set; }

        private IAccountState _state;

        public AccountState Status
        {
            get { return _state.Status; }
        }

        public BankAccount(int id, decimal balance = 0.0M)
        {
            Id = id;
            Balance = balance;
            
            _state = UpdateAccountState();
        }

        public void Deposit(decimal amount)
        {
            Contract.Assert(amount > 0.0M);

            _state.Deposit(amount, this);

            _state = UpdateAccountState();
        }

        public void Withdraw(decimal amount)
        {
            Contract.Assert(amount > 0.0M);

           _state.Withdraw(amount, this);
                
           _state = UpdateAccountState();            
        }

        public void PayInterest()
        {
            _state.PayInterest(this);
                
        }

        private IAccountState UpdateAccountState()
        {
            if (Balance >= 0)
                return BankAccount._normalState;
            else
                return BankAccount._overdraftState;                
        }

        private static IAccountState _normalState = new NormalState();
        private static IAccountState _overdraftState = new OverdraftState();

    }
}