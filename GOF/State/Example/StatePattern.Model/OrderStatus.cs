﻿namespace StatePattern.Model
{
    public enum OrderStatus
    {
       New = 0,
       Shipped = 1,
       Canceled = 2
    }
}
