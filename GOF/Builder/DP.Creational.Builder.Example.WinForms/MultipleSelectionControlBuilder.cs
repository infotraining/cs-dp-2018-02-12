﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DP.Creational.Builder.Example.WinForms
{
    public interface MultipleSelectionControlBuilder
    {
        MultipleSelectionControlBuilder AddItem(string item);
        Control GetControl();
    }

    public class ComboBoxBuilder : MultipleSelectionControlBuilder
    {
        FlowLayoutPanel pnlLayout = new FlowLayoutPanel();
        ComboBox cmbControl = new ComboBox();


        public ComboBoxBuilder(string title)
        {
            cmbControl.Name = "cmb" + title;
            Label lblTitle = new Label();
            lblTitle.Text = title;
            pnlLayout.Controls.Add(lblTitle);
            pnlLayout.Controls.Add(cmbControl);
        }

        public MultipleSelectionControlBuilder AddItem(string item)
        {
            cmbControl.Items.Add(item);

            return this;
        }

        public Control GetControl()
        {
            return pnlLayout;
        }
    }

    public class CheckBoxBuilder : MultipleSelectionControlBuilder
    {
        GroupBox gbxControl = new GroupBox();
        FlowLayoutPanel pnlLayout = new FlowLayoutPanel();

        public CheckBoxBuilder(string title)
        {
            gbxControl.Text = title;
            gbxControl.AutoSize = true;
            gbxControl.Dock = DockStyle.Fill;
            gbxControl.Margin = new System.Windows.Forms.Padding(10);
            pnlLayout.AutoSize = true;
            pnlLayout.Dock = DockStyle.Fill;
            gbxControl.Controls.Add(pnlLayout);
        }

        public MultipleSelectionControlBuilder AddItem(string item)
        {
            CheckBox cbxItem = new CheckBox();
            cbxItem.Name = "cbx" + item;
            cbxItem.Text = item;
            pnlLayout.Controls.Add(cbxItem);
            //gbxControl.Controls.Add(cbxItem);
            
            return this;
        }

        public Control GetControl()
        {
            return gbxControl;
        }
    }


    public class RadioButtonBuilder : MultipleSelectionControlBuilder
    {
        GroupBox gbxControl = new GroupBox();
        FlowLayoutPanel pnlLayout = new FlowLayoutPanel();

        public RadioButtonBuilder(string title)
        {
            gbxControl.Text = title;
            gbxControl.AutoSize = true;
            gbxControl.Dock = DockStyle.Fill;
            gbxControl.Margin = new System.Windows.Forms.Padding(10);
            pnlLayout.AutoSize = true;
            pnlLayout.Dock = DockStyle.Fill;
            gbxControl.Controls.Add(pnlLayout);
        }

        public MultipleSelectionControlBuilder AddItem(string item)
        {
            RadioButton rdbItem = new RadioButton();
            rdbItem.Name = "cbx" + item;
            rdbItem.Text = item;
            pnlLayout.Controls.Add(rdbItem);

            return this;
        }

        public Control GetControl()
        {
            return gbxControl;
        }
    }
}
