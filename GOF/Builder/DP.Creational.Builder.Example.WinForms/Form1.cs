﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DP.Creational.Builder.Example.WinForms
{
    public partial class Form1 : Form
    {
        MultipleSelectionControlBuilder selectionBuilder;
        
        public Form1()
        {
            InitializeComponent();

            selectionBuilder = new ComboBoxBuilder("Multiple selection control test:");

            selectionBuilder.AddItem("One");
            selectionBuilder.AddItem("Two");
            selectionBuilder.AddItem("Three").AddItem("Four");

            pnlMain.Controls.Add(selectionBuilder.GetControl());
        }
    }
}
