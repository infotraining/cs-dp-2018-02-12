﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Drawing;

namespace DP.Creational.FactoryMethod.Example
{    
    public class ShapeGroup : IShape
    {
        List<IShape> _shapes = new List<IShape>();

        public void Draw()
        {
            foreach(var shape in _shapes)
                shape.Draw();
        }

        public void Move(int dx, int dy)
        {
            foreach (var shape in _shapes)
            {
                shape.Move(dx, dy);
            }
        }

        public void Add(IShape shape)
        {
            _shapes.Add(shape);
        }

        public class Creator : IShapeCreator
        {
            private ShapeFactory _shapeFactory;

            public Creator(ShapeFactory shapeFactory)
            {
                _shapeFactory = shapeFactory;
            }

            public IShape CreateShape(params object[] args)
            {
                ShapeGroup shapeGroup = new ShapeGroup();

                XElement shapeGroupElements = (XElement)args[0];

                foreach (XElement element in shapeGroupElements.Elements("Shape"))
                {
                    string id = element.Attribute("Id").Value;

                    IShape s = _shapeFactory.Create(id, element);

                    shapeGroup.Add(s);                    
                }

                return shapeGroup;
            }
        }
    }
}
