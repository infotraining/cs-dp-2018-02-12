﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Drawing
{
    public class Rectangle : Shape
    {
        private int width;
        public int Width 
        { 
            get
            {
                return width;
            }

            set
            {
                width = value;
                int x = Points[0].X;
                int y = Points[0].Y;
                Points[1] = new Point(x + width, y);
                Points[2] = new Point(x + width, y + Height);
                Points[3] = new Point(x, y + Height);
            }
        }

        private int height;
        public int Height
        {
            get 
            { 
                return height; 
            }

            set
            {
                height = value;
                int x = Points[0].X;
                int y = Points[1].Y;

                Points[1] = new Point(x + Width, y);
                Points[2] = new Point(x + Width, y + height);
                Points[3] = new Point(x, y + height);
            }
        }

        public Rectangle() : this(0, 0, 0, 0)
        {
        }

        public Rectangle(int x, int y, int width, int height)
        {
            AddPoint(new Point(x, y));
            AddPoint(new Point(x + width, y));
            AddPoint(new Point(x + width, y + height));
            AddPoint(new Point(x, y + height));
        }

        public override void Draw()
        {
            Console.Write("Drawing a rectangle ");
            for (int i = 0; i < NumberOfPoints(); ++i)
                Console.Write("{0} ", Points[i]);
            Console.WriteLine();
        }

        public class Creator : IShapeCreator
        {
            IShape IShapeCreator.CreateShape(params object[] args)
            {
                XElement rectElement = (XElement)args[0];
                int x = int.Parse(rectElement.Element("Point").Element("X").Value);
                int y = int.Parse(rectElement.Element("Point").Element("Y").Value);
                int width = int.Parse(rectElement.Element("Width").Value);
                int height = int.Parse(rectElement.Element("Height").Value);

                return new Rectangle(x, y, width, height);
            }
        }

    }
}