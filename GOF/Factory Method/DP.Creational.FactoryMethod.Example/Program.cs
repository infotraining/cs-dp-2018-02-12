﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Drawing;
using System.Xml.Linq;

namespace DP.Creational.FactoryMethod.Example
{
    public class Document
    {
        private readonly ShapeFactory _shapeFactory;
        private ShapeGroup _shapes;

        public Document(ShapeFactory shapeFactory)
        {
            _shapeFactory = shapeFactory;
        }

        public virtual void Load(string path)
        {
            XElement root = XElement.Load(path);

            _shapes = (ShapeGroup)_shapeFactory.Create("ShapeGroup", root);
        }

        public void Show()
        {
            _shapes.Draw();
        }
    }

    class Program
    {
        static void InitShapeFactory(ShapeFactory shapeFactory)
        {
            shapeFactory.Register("Circle", new Circle.Creator());
            shapeFactory.Register("Rectangle", new Rectangle.Creator());
            shapeFactory.Register("ShapeGroup", new ShapeGroup.Creator(shapeFactory));
        }

        static void Main(string[] args)
        {
            ShapeFactory shapeFactory = new ShapeFactory();
            InitShapeFactory(shapeFactory);
            Document doc = new Document(shapeFactory);
            doc.Load("../../graphics.xml");
            doc.Show();
        }
    }
}
