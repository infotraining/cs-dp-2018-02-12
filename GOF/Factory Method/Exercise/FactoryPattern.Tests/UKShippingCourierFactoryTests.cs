﻿using FactoryPattern.Model;
using NUnit.Framework;

namespace FactoryPattern.Tests
{
    [TestFixture]
    public class UKShippingCourierFactoryTests
    {
        UKShippingCourierFactory ukShippingCourierFactory = new UKShippingCourierFactory();

        [SetUp]
        public void Setup()
        {
            ukShippingCourierFactory.Register(() => new DHL(), order => order.TotalCost > 100 || order.WeightInKG > 5);
            ukShippingCourierFactory.Register(() => new RoyalMail(), order => order.TotalCost <= 100 || order.WeightInKG <= 5);
        }

        [Test]
        public void UKShippingCourierFactory_Should_Create_DHL_Shipping_Courier_For_An_Order_With_A_TotalCost_Of_Over_100()
        {
            Order order = new Order() { TotalCost = 101};            
                        
            IShippingCourier courier = ukShippingCourierFactory.CreateShippingCourier(order);

            Assert.That(courier, Is.TypeOf(typeof(DHL)));
        }

        [Test]
        public void UKShippingCourierFactory_Should_Create_DHL_Shipping_Courier_For_An_Order_With_A_Weight_In_KG_Over_5()
        {
            Order order = new Order() { WeightInKG = 6 };

            IShippingCourier courier = ukShippingCourierFactory.CreateShippingCourier(order);

            Assert.That(courier, Is.TypeOf(typeof(DHL)));
        }

        [Test]
        public void UKShippingCourierFactory_Should_Create_RoyalMail_Shipping_Courier_For_An_Order_With_A_Weight_In_KG_5_And_Under()
        {
            Order order = new Order() { WeightInKG = 5 };

            IShippingCourier courier = ukShippingCourierFactory.CreateShippingCourier(order);

            Assert.That(courier, Is.TypeOf(typeof(RoyalMail)));
        }

        [Test]
        public void UKShippingCourierFactory_Should_Create_DHL_Shipping_Courier_For_An_Order_With_A_TotalCost_Of_100_And_Under()
        {
            Order order = new Order() { TotalCost = 100 };

            IShippingCourier courier = ukShippingCourierFactory.CreateShippingCourier(order);

            Assert.That(courier, Is.TypeOf(typeof(RoyalMail)));
        }
    }
}
