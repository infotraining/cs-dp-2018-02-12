﻿namespace FactoryPattern.Model
{
    public class OrderService
    {
        private UKShippingCourierFactory _ukShippingCourierFactory;

        public OrderService(UKShippingCourierFactory ukShippingCourierFactory)
        {
            _ukShippingCourierFactory = ukShippingCourierFactory;
        }

        public void Dispatch(Order order)
        {
            IShippingCourier shippingCourier = _ukShippingCourierFactory.CreateShippingCourier(order);

            order.CourierTrackingId = shippingCourier.GenerateConsignmentLabelFor(order.DispatchAddress);    
        }
    }
}
