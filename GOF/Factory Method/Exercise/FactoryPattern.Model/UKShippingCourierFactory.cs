﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace FactoryPattern.Model
{
    //public interface ICourierCreator
    //{
    //    IShippingCourier CreateShippingCourier();
    //}

    //public class DHLCreator : ICourierCreator
    //{
    //    public IShippingCourier CreateShippingCourier()
    //    {
    //        return new DHL();
    //    }
    //}

    //public class RoyalMailCreator : ICourierCreator
    //{
    //    public IShippingCourier CreateShippingCourier()
    //    {
    //        return new RoyalMail();
    //    }
    //}

    public class SpecifiedShippingCreator
    {
        public Func<Order, bool> IsSpecifiedForShipping { get; set; }
        public Func<IShippingCourier> CourierCreator { get; set; }
    }

    public class UKShippingCourierFactory
    {
        private IList<SpecifiedShippingCreator> _creators = new List<SpecifiedShippingCreator>();

        public IShippingCourier CreateShippingCourier(Order order)
        {
            var courierCreator = _creators.First(sc => sc.IsSpecifiedForShipping(order)).CourierCreator;
            return courierCreator();
        }

        public void Register(Func<IShippingCourier> creator, Func<Order, bool> specification)
        {
            _creators.Add(new SpecifiedShippingCreator() {CourierCreator = creator, IsSpecifiedForShipping = specification});
        }
    }
}
