﻿namespace FactoryPattern.Model
{
    public class RoyalMail : IShippingCourier 
    {                
        public string GenerateConsignmentLabelFor(Address address)
        {
            return "RMXXXX-XXXX-XXXX";
        }        
    }
}
