﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace StrategyPattern.Model
{
    public interface IDiscountCalculator
    {
        decimal CalculateDiscount(Basket basket);
    }

    class MoneyOffDiscount : IDiscountCalculator
    {
        public decimal CalculateDiscount(Basket basket)
        {
            if (basket.TotalCost > 100)
                return basket.TotalCost - 10m;
            if (basket.TotalCost > 50)
                return basket.TotalCost - 5m;
            else
                return basket.TotalCost;
        }
    }

    class PercentageOffDiscount : IDiscountCalculator
    {
        public decimal CalculateDiscount(Basket basket)
        {
            return basket.TotalCost * 0.85m;
        }
    }

    class ZeroDiscount : IDiscountCalculator
    {
        public decimal CalculateDiscount(Basket basket)
        {
            return basket.TotalCost;
        }
    }

    class VipDiscount : IDiscountCalculator
    {
        public decimal CalculateDiscount(Basket basket)
        {
            return 0.2m * basket.TotalCost;
        }
    }

    public class Basket
    {
        private IDiscountCalculator _discountCalculator;

        public Basket(DiscountType discountType)
        {
            _discountCalculator = DiscountFactory.Create(discountType);
        }

        public decimal TotalCost { get; set; }        

        public decimal GetTotalCostAfterDiscount()
        {
            return _discountCalculator.CalculateDiscount(this);
        }
    }

    public static class DiscountFactory
    {
        private static Dictionary<DiscountType, IDiscountCalculator> _discounts;

        static DiscountFactory()
        {
            foreach (var item in Enum.GetValues(typeof(DiscountType)))
            {
                string typeName = item.ToString() + "Discount";

                var assembly = Assembly.LoadFrom("StrategyPattern.Model");

                var types = assembly.GetTypes();
                var discountStrategyType = types.First(t => t.Name == typeName);

                IDiscountCalculator discount = (IDiscountCalculator)Activator.CreateInstance(discountStrategyType);

                _discounts.Add((DiscountType)item, discount);
            }
        }

        public static IDiscountCalculator Create(DiscountType discountType)
        {
            return _discounts[discountType];
        }
    }
}
