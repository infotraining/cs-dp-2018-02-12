﻿namespace StrategyPattern.Model
{
    public enum DiscountType
    {
        Zero = 0,
        MoneyOff = 1,
        PercentageOff = 2
    }
}
