﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Structural.Proxy.Example
{
    class SubjectAccessor
    {
        public interface ISubject
        {
            string Request();
        }

        private class Subject
        {
            public string Request()
            {
                return "Subject Requested " + " | Operation performed\n";
            }
        }

        public class Proxy : ISubject
        {
            Subject subject;

            #region ISubject Members

            public string Request()
            {
                // virual proxy - lazy instantiation
                if (subject == null)
                {
                    Console.WriteLine("Subject inactive");
                    subject = new Subject();
                }
                Console.WriteLine("Subject active");

                return "Proxy: Call to " + subject.Request();
            }

            #endregion
        }

        public class ProtectionProxy : ISubject
        {
            Subject subject;
            string password = "Abracadabra";

            public string Authenticate(string supplied)
            {
                if (supplied != password)
                    return "Protection Proxy: No access";
                else
                    subject = new Subject();

                return "Protection Proxy: Authenticated";
            }

            #region ISubject Members

            public string Request()
            {
                // protection proxy - autentication before Request required
                if (subject == null)
                    return "Protection Proxy: Authenticate first";
                else
                    return "Protection Proxy: Call to " + subject.Request();
            }

            #endregion
        }
    }

    class Client : SubjectAccessor
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Virtual Proxy Pattern\n");

            ISubject subject = new Proxy();
            Console.WriteLine(subject.Request());
            Console.WriteLine(subject.Request());

            Console.WriteLine("\nProtection Proxy Pattern\n");

            subject = new ProtectionProxy();
            Console.WriteLine(subject.Request());
            Console.WriteLine((subject as ProtectionProxy).Authenticate("Secret"));
            Console.WriteLine((subject as ProtectionProxy).Authenticate("Abracadabra"));
            Console.WriteLine(subject.Request());

            Console.ReadKey();
        }
    }
}
