﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Structural.Decorator.Exercise
{
    class Program
    {
        static void Main(string[] args)
        {
            ICoffee myCoffee = new WhippedCream(new Whisky(new ExtraEspresso(new Espresso())));

            Console.WriteLine("Coffee: {0} - Price: {1:c}", myCoffee.Description, myCoffee.Price);
            myCoffee.Prepare();

            Console.WriteLine();

            CoffeeBuilder cb = new CoffeeBuilder();

            cb.CreateBase<Espresso>()
                .Add<Whisky>().Add<Whisky>().Add<WhippedCream>();

            myCoffee = cb.GetCoffee();

            Console.WriteLine("Coffee: {0} - Price: {1:c}", myCoffee.Description, myCoffee.Price);
            myCoffee.Prepare();

            Console.ReadKey();
        }
    }
}
