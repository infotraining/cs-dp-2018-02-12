﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Structural.Decorator.Exercise
{
    /// <summary>
    ///  Coffee
    /// </summary>
    public interface ICoffee
    {
        double Price { get; set; }
        string Description { get; set; }

        void Prepare();
    }

    /// <summary>
    /// Espresso
    /// </summary>
    public class Espresso : ICoffee
    {
        public Espresso()
        {
            Price = 4.0;
            Description = "Espresso";
        }

        public double Price { get; set; }
        public string Description { get; set; }

        public void Prepare()
        {
            Console.WriteLine("Making a perfect Espresso: 7g, 15bar, 95degC & 25sec");
        }
    }

    /// <summary>
    /// Cappuccino
    /// </summary>
    public class Cappuccino : ICoffee
    {
        public Cappuccino()
        {
            Price = 6.0;
            Description = "Cappuccino";
        }

        public double Price { get; set; }
        public string Description { get; set; }

        public void Prepare()
        {
            Console.WriteLine("Making a perfect Cappuccino");
        }
    }

    /// <summary>
    /// Latte
    /// </summary>
    public class Latte : ICoffee
    {
        public Latte()
        {
            Price = 8.0;
            Description = "Latte";
        }

        public double Price { get; set; }
        public string Description { get; set; }

        public void Prepare()
        {
            Console.WriteLine("Making a superb Latte");
        }
    }

    public abstract class CoffeeDecorator : ICoffee
    {
        private double _price;
        private string _description;

        public ICoffee Coffee { get; set; }

        internal CoffeeDecorator()
        {            
        }

        public CoffeeDecorator(ICoffee coffee)
        {
            Coffee = coffee;
        }

        public double Price
        {
            get { return Coffee.Price + _price; }
            set { _price = value; }
        }

        public string Description
        {
            get { return Coffee.Description + " + " + _description; }
            set { _description = value; }
        }

        public virtual void Prepare()
        {
            Coffee.Prepare();
        }
    }

    public class WhippedCream : CoffeeDecorator
    {
        internal WhippedCream()
        {
            Price = 2.5;
            Description = "Whipped cream";
        }

        public WhippedCream(ICoffee coffee) : base(coffee)
        {
            Price = 2.5;
            Description = "Whipped cream";
        }

        public override void Prepare()
        {
            base.Prepare();
            Console.WriteLine("Adding whipped cream...");
        }
    }

    public class Whisky : CoffeeDecorator
    {
        internal Whisky()
        {
            Price = 6.0;
            Description = "Whisky";
        }

        public Whisky(ICoffee coffee) : base(coffee)
        {
            Price = 6.0;
            Description = "Whisky";
        }

        public override void Prepare()
        {
            base.Prepare();
            Console.WriteLine("Pouring 5cl of whisky");
        }
    }

    public class ExtraEspresso : CoffeeDecorator
    {
        internal ExtraEspresso()
        {
            Price = 4.0;
            Description = "Extra Espresso";
        }

        public ExtraEspresso(ICoffee coffee) : base(coffee)
        {
            Price = 4.0;
            Description = "Extra Espresso";
        }

        public override void Prepare()
        {
            base.Prepare();

            new Espresso().Prepare();
        }
    }


    class CoffeeBuilder
    {
        private ICoffee _coffee;

        public CoffeeBuilder CreateBase<TBase>() where TBase : ICoffee, new()
        {
            _coffee = new TBase();

            return this;
        }

        public CoffeeBuilder Add<TCondiment>() where TCondiment : CoffeeDecorator
        {
            CoffeeDecorator decorated = (CoffeeDecorator)Activator.CreateInstance(typeof(TCondiment), nonPublic: true);
            decorated.Coffee = _coffee;
            _coffee = decorated;

            return this;
        }

        public ICoffee GetCoffee()
        {
            return _coffee;
        }
    }

}
