﻿namespace DecoratorPattern.Model
{
    public class Product
    {        
        public IPrice Price { get; set; }
    }
}
