﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Behavioral.Chain.Example
{
    
    [Flags]
    public enum LogEntryType
    {
        Warning = 1, Info = 2, Error = 4
    }

    public class Logger : ILogger
    {
        private List<ILogHandler> _logHandlers = new List<ILogHandler>();

        public Logger(ILogHandler logHandler)
        {
            this._logHandlers.Add(logHandler);
        }

        public void Log(string msg, LogEntryType logEntryType)
        {
            foreach (var logHandler in _logHandlers)
            {
                if (logHandler.CanHandle(logEntryType))
                {
                    logHandler.WriteLogMessage(msg);
                    return;
                }
            }

            throw new LogEntryUnhandledException(string.Format("Error: {0} was not logged in", msg));
        }

        public void RegisterLogHandler(ILogHandler logHandler)
        {
            _logHandlers.Insert(0, logHandler);
        }
    }

    public class LogEntryUnhandledException : Exception
    {
        public LogEntryUnhandledException(string message) : base(message)
        {
        }
    }

    public interface ILogger
    {
        void Log(string msg, LogEntryType logEntryType);
        void RegisterLogHandler(ILogHandler fakeLogHandler);
    }

    public interface ILogHandler
    {
        void WriteLogMessage(string msg);
        bool CanHandle(LogEntryType logEntryType);
    }

    public abstract class LogHandler : ILogHandler
    {
        private LogEntryType _logLevel;

        public LogHandler(LogEntryType logEntryType)
        {
            _logLevel = logEntryType;
        }

        public abstract void WriteLogMessage(string msg);

        public bool CanHandle(LogEntryType logEntryType)
        {
            return (logEntryType & _logLevel) != 0;
        }
    }

    public class ConsoleInfoLogHandler : LogHandler
    {
        public ConsoleInfoLogHandler(LogEntryType logEntryType) : base(logEntryType)
        {
        }

        public override void WriteLogMessage(string msg)
        {
            Console.WriteLine("Info: " + msg);
        }
    }

    public class FileErrorLogHandler : LogHandler
    {
        public string LogFileName { get; private set; }

        public FileErrorLogHandler(string logFileName) : base(LogEntryType.Error)
        {
            LogFileName = logFileName;
        }

        public override void WriteLogMessage(string msg)
        {
            using (var sw = File.AppendText(LogFileName))
            {
                sw.WriteLine("Error: " + msg);
            }
        }
    }

    public class MemoryLogHandler : ILogHandler
    {
        readonly List<string> _logs = new List<string>();

        public void WriteLogMessage(string msg)
        {
            _logs.Add("Warning: " + msg);
        }

        public bool CanHandle(LogEntryType logEntryType)
        {
            return true;
        }

        public void FlushToConsole()
        {
            foreach (var log in _logs)
            {
                Console.WriteLine(log);
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var defaultLogger= new MemoryLogHandler();

            ILogger logger = new Logger(defaultLogger);
            logger.RegisterLogHandler(new FileErrorLogHandler("errors.log"));
            logger.RegisterLogHandler(new ConsoleInfoLogHandler(LogEntryType.Info));

            logger.Log("Log1", LogEntryType.Info);
            logger.Log("Log2", LogEntryType.Error);
            logger.Log("Log3", LogEntryType.Warning);
            logger.Log("Log4", LogEntryType.Error);
            logger.Log("Log5", LogEntryType.Warning);
            logger.Log("Log6", LogEntryType.Info);

            defaultLogger.FlushToConsole();
        }
    }
}
