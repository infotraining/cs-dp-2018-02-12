﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DP.Structural.Adapter.Pluggable
{
    class ControlTreeContentProvider : ITreeContentProvider<Control>
    {
        #region ITreeContentProvider Members

        public IEnumerable<Control> GetChildren(Control node)
        {
            Control[] children = new Control[node.Controls.Count];
            node.Controls.CopyTo(children, 0);
            return children;
        }

        public Control GetParent(Control node)
        {
            return node.Parent;

        }

        public bool HasChildren(Control node)
        {
            if (node.Controls.Count > 0)
                return true;

            return false;
        }

        public string GetText(Control node)
        {
            string separator = " - ";
            if (string.IsNullOrEmpty(node.Text))
                separator = "";    
            return node.Text + separator + node.GetType().Name;
        }

        #endregion
    }
}
