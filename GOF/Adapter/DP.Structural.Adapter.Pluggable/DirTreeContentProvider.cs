﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DP.Structural.Adapter.Pluggable
{
    class DirTreeContentProvider : ITreeContentProvider<DirectoryInfo>
    {
        #region ITreeContentProvider Members

        public IEnumerable<DirectoryInfo> GetChildren(DirectoryInfo node)
        {
            return node.GetDirectories();
        }

        public DirectoryInfo GetParent(DirectoryInfo node)
        {
            return node.Parent;
        }

        public bool HasChildren(DirectoryInfo node)
        {
            if (node.GetDirectories().Length == 0)
                return false;

            return true;
        }

        public string GetText(DirectoryInfo node)
        {
            return node.Name;
        }

        #endregion
    }
}
