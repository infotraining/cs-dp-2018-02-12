﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DP.Structural.Adapter.Pluggable
{
    public class TreeViewer<T>
    {
        ITreeContentProvider<T> provider;
        TreeView treeView;

        public TreeViewer(TreeView tv)
        {
            treeView = tv;
        }

        public void SetTreeContentProvider(ITreeContentProvider<T> provider)
        {
            this.provider = provider;
        }

        public void BuildTree(TreeNode tn, T node)
        {
            if (tn == null)
            {
                tn = new TreeNode(provider.GetText(node));
                treeView.Nodes.Add(tn);
            }

            if (provider.HasChildren(node))
            {
                IEnumerable<T> children = provider.GetChildren(node);
                foreach (T child in children)
                {
                    TreeNode childNode = new TreeNode(provider.GetText(child));
                    tn.Nodes.Add(childNode);
                    BuildTree(childNode, child);
                }
            }
        }
    }
}
