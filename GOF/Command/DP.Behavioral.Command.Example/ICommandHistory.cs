﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Behavioral.Command.Example
{
    public interface ICommandHistory
    {
        void Push(IUndoableCommand command);
        IUndoableCommand Pop();
    }

    public class CommandHistory : ICommandHistory
    {
        Stack<IUndoableCommand> _history = new Stack<IUndoableCommand>();

        public void Push(IUndoableCommand command)
        {
            _history.Push(command);
        }

        public IUndoableCommand Pop()
        {
            return _history.Pop();
        }
    }

}
