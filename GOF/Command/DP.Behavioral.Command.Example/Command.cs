﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DP.Behavioral.Command.Example;

namespace DP.Behavioral.Command.Example
{
    public interface ICommand
    {
        void Execute();
    }

    public interface IUndoableCommand : ICommand
    {        
        void Undo();
        IUndoableCommand Clone();
    }

    public abstract class UndoableCommand : IUndoableCommand
    {
        private ICommandHistory _commandHistory;        

        protected ICommandHistory CommandHistory
        {
            get { return _commandHistory; }
        }

        public UndoableCommand(ICommandHistory commandHistory)
        {
            _commandHistory = commandHistory;            
        }

        public virtual IUndoableCommand Clone()
        {
            return (IUndoableCommand)MemberwiseClone();
        }

        // template method
        public void Execute()
        {
            if (CanExecute())
            {
                DoSaveState();
                CommandHistory.Push(this.Clone());
                DoExecute();
            }            
        }        

        public void Undo()
        {            
            DoUndo();
        }

        protected virtual bool CanExecute()
        {
            return true;
        }

        protected abstract void DoUndo();
        protected abstract void DoExecute();
        protected abstract void DoSaveState();
    }    


    public class CopyCommand : ICommand
    {
        private TextBox receiver;

        public CopyCommand(TextBox receiver)
        {
            this.receiver = receiver;
        }

        #region ICommand Members

        public void Execute()
        {
            receiver.Copy();
        }

        #endregion
    }

    public class PasteCommand : UndoableCommand
    {
        string before;

        private TextBox receiver;

        public PasteCommand(TextBox receiver, ICommandHistory commandHistory)
            : base(commandHistory)
        {
            this.receiver = receiver;
        }

        protected override void DoSaveState()
        {
            before = receiver.Text;
        }

        protected override void DoExecute()
        {
            receiver.Paste();            
        }
       
        protected override void DoUndo()
        {
            receiver.Text = before;
        }
    }

    public class ToUpperCommand : UndoableCommand
    {
        protected TextBox receiver;
        protected string selectedText;
        protected int selectionStarts;
        protected int selectionLength;

        public ToUpperCommand(TextBox receiver, ICommandHistory commandHistory)
            : base(commandHistory)
        {
            this.receiver = receiver;
        }

        protected override bool CanExecute()
        {
            return receiver.SelectionLength > 0;
        }

        protected override void DoSaveState()
        {
            selectedText = receiver.SelectedText;
            selectionStarts = receiver.SelectionStart;           
            selectionLength = receiver.SelectionLength;
        }

        protected override void DoExecute()
        {            
            receiver.Text = receiver.Text.Remove(selectionStarts, selectionLength);
            receiver.Text = receiver.Text.Insert(selectionStarts, selectedText.ToUpper());
        }        

        protected override void DoUndo()
        {
            var text = receiver.Text.Remove(selectionStarts, selectionLength);
            receiver.Text = text.Insert(selectionStarts, selectedText);
        }
    }

    public class ToLowerCommand : ToUpperCommand
    {

        public ToLowerCommand(TextBox receiver, ICommandHistory commandHistory)
            : base(receiver, commandHistory)
        {
            this.receiver = receiver;
        }

        protected override void DoExecute()
        {

            receiver.Text = receiver.Text.Remove(selectionStarts, selectionLength);
            receiver.Text = receiver.Text.Insert(selectionStarts, selectedText.ToLower());
        }        
    }

    public class RemoveCommand : UndoableCommand
    {
        private TextBox receiver;
        private string selectedText;
        private int selectionStarts;
        private int selectionLength;

        public RemoveCommand(TextBox receiver, ICommandHistory commandHistory)
            : base(commandHistory)
        {
            this.receiver = receiver;
        }

        protected override bool CanExecute()
        {
            return receiver.SelectionLength > 0;
        }

        protected override void DoSaveState()
        {
            selectionStarts = receiver.SelectionStart;
            selectionLength = receiver.SelectionLength;            
            selectedText = receiver.SelectedText;            
        }

        protected override void DoExecute()
        {
            receiver.Text = receiver.Text.Remove(selectionStarts, selectionLength);            
        }        

        protected override void DoUndo()
        {
            var text = receiver.Text;
            receiver.Text = text.Insert(selectionStarts, selectedText);
        }
    }

    public class UndoCommand : ICommand
    {
        ICommandHistory _commandHistory;

        public UndoCommand(ICommandHistory commandHistory)
        {
            _commandHistory = commandHistory;
        }

        public void Execute()
        {
            IUndoableCommand lastCmd = _commandHistory.Pop();
            lastCmd.Undo();
        }        
    }
}