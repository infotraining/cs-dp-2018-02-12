﻿using System;
using System.Runtime.Remoting.Messaging;

namespace TemplateMethodPattern.Model
{
    public abstract class ReturnProcessTemplate
    {       
        protected abstract void GenerateReturnTransactionFor(ReturnOrder returnOrder);
        protected abstract void CalculateRefundFor(ReturnOrder returnOrder);
        protected abstract bool CheckValidity(ReturnOrder returnOrder);
        protected virtual void Log(string invalidReturnOrder)
        {
        }

        public void Process(ReturnOrder returnOrder)
        {
            if (!CheckValidity(returnOrder))
            {
                Log("Invalid return order");
            }
            GenerateReturnTransactionFor(returnOrder);
            CalculateRefundFor(returnOrder);
        }
    }
}
