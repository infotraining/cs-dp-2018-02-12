﻿using System.Collections.Generic;

namespace DependencyInversionPrinciple.Model
{
    public class ProductService
    {
        private IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;            
        }

        public IEnumerable<Product> GetProductsWithDiscount(IProductDiscountStrategy discount)
        {
            IEnumerable<Product> products = _productRepository.FindAll();

            foreach (Product p in products)
                p.AdjustPriceWith(discount);

            return products;
        }

        public IEnumerable<Product> GetTop10ProductsWithDiscount(IProductDiscountStrategy discount)
        {
            IEnumerable<Product> products = _productRepository.FindMostPopular(10);

            foreach (Product p in products)
                p.AdjustPriceWith(discount);

            return products;
        }
    }
}
