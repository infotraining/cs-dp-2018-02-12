﻿using System;

namespace LiskovSubstitutionPrinciple
{
    public class PaymentServiceFactory
    {
        public static PaymentServiceBase GetPaymentServiceFrom(PaymentType paymentType)
        {
            switch (paymentType)
            {
                case PaymentType.PayPal:
                    return new PayPalPayment("Scott123-PP", "pass1");
                case PaymentType.WorldPay:
                    return new WorldPayPayment("Scott134-WPP", "pass2", "A1");
                default:
                    throw new ApplicationException("No Payment Service available for " + paymentType.ToString());
            }
        }
    }
}