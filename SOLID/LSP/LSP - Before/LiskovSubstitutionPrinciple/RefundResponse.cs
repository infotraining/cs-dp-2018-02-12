﻿namespace LiskovSubstitutionPrinciple
{
    public class RefundResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}