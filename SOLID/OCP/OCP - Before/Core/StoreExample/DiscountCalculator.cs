﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Core.StoreExample
{
    interface IDiscountRule
    {
        decimal CalculateCustomerDiscount(Customer customer);
    }

    class SeniorDiscountRule : IDiscountRule
    {
        public decimal CalculateCustomerDiscount(Customer customer)
        {
            if (customer.DateOfBirth < DateTime.Now.AddYears(-65))
                return 0.05m;

            return 0.0m;
        }
    }

    class VeteranRule : IDiscountRule
    {
        public decimal CalculateCustomerDiscount(Customer customer)
        {
            if (customer.IsVeteran)
            {
                // veterans get 10%
                return .10m;
            }

            return 0.0m;
        }
    }

    class BirthdayDiscountRule : IDiscountRule
    {
        public decimal CalculateCustomerDiscount(Customer customer)
        {
            if (customer.DateOfBirth.Month == DateTime.Now.Month &&
                customer.DateOfBirth.Day == DateTime.Now.Day)
            {
                return 0.1m;
            }

            return 0.0m;
        }
    }

    class FirstTimeCustomerRule : IDiscountRule
    {
        public decimal CalculateCustomerDiscount(Customer customer)
        {
            if (!customer.DateOfFirstPurchase.HasValue)
            {
                return 0.15m;
            }

            return 0.0m;
        }
    }

    class LoyalCustomerRule : IDiscountRule
    {
        private decimal _discount;
        private int _yearsAsCustomer;

        public LoyalCustomerRule(int yearsAsCustomer, decimal discount)
        {
            _discount = discount;
            _yearsAsCustomer = yearsAsCustomer;
        }

        public decimal CalculateCustomerDiscount(Customer customer)
        {
            if (customer.DateOfFirstPurchase.HasValue)
            {
                if (customer.DateOfFirstPurchase.Value.AddYears(_yearsAsCustomer) <= DateTime.Today)
                {                    
                    return _discount;
                }
            }

            return 0.0m;
        }
    }

    public class DiscountCalculator
    {   
        List<IDiscountRule> _rules = new List<IDiscountRule>();

        public DiscountCalculator()
        {
            _rules.Add(new SeniorDiscountRule());
            _rules.Add(new VeteranRule());
            _rules.Add(new FirstTimeCustomerRule());
            _rules.Add(new LoyalCustomerRule(1, 0.1m));
            _rules.Add(new LoyalCustomerRule(5, 0.12m));
            _rules.Add(new LoyalCustomerRule(10, 0.20m));
            _rules.Add(new BirthdayDiscountRule());
        }

        public decimal CalculateDiscountPercentage(Customer customer)
        {
            return _rules.Max(r => r.CalculateCustomerDiscount(customer));                           
        }
    }
  
    public class Customer
    {
        public DateTime? DateOfFirstPurchase { get; set; }
        public DateTime DateOfBirth { get; set; }
        public bool IsVeteran { get; set; }
    }
}
