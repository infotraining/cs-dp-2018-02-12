﻿using System;
using System.Net.Mail;
using CommerceProject.Utility;

namespace CommerceProject.Model
{
    public interface INotificationService
    {
        void NotifyCustomerOrder(Cart cart);
    }

    public class SmtpNotificationService : INotificationService
    {
        public void NotifyCustomerOrder(Cart cart)
        {
            string customerEmail = cart.CustomerEmail;
            if (!String.IsNullOrEmpty(customerEmail))
            {
                using (var message = new MailMessage("orders@somewhere.com", customerEmail))
                using (var client = new SmtpClient("localhost"))
                {
                    message.Subject = "Your order placed on " + DateTime.Now.ToString();
                    message.Body = "Your order details: \n " + cart.ToString();

                    try
                    {
                        client.Send(message);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Problem sending notification email", ex);
                    }
                }
            }
        }
    }
}