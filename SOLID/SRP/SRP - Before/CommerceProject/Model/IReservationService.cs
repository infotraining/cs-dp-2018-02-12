﻿using System;
using System.Collections.Generic;
using CommerceProject.Services;

namespace CommerceProject.Model
{
    public interface IReservationService
    {
        void ReserveInventory(IEnumerable<OrderItem> cartItems);
    }

    class ReservationService : IReservationService
    {
        public void ReserveInventory(IEnumerable<OrderItem> cartItems)
        {
            foreach (var item in cartItems)
            {
                try
                {
                    var inventorySystem = new InventorySystem();
                    inventorySystem.Reserve(item.Sku, item.Quantity);

                }
                catch (InsufficientInventoryException ex)
                {
                    throw new OrderException("Insufficient inventory for item " + item.Sku, ex);
                }
                catch (Exception ex)
                {
                    throw new OrderException("Problem reserving inventory", ex);
                }
            }
        }
    }
}