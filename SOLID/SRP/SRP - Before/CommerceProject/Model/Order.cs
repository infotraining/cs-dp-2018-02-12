﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using CommerceProject.Services;
using CommerceProject.Utility;

namespace CommerceProject.Model
{
    public abstract class OrderBase
    {
        protected readonly Cart _cart;

        protected OrderBase(Cart cart)
        {
            _cart = cart;
        }

        public virtual void Checkout()
        {

        }
    }

    public class OnlineOrder : OrderBase
    {
        private INotificationService _notificationService;
        private PaymentDetails _paymentDetails;
        private IPaymentProcessor _paymentProcessor;
        private IReservationService _reservationService;

        public OnlineOrder(Cart cart, INotificationService notificationService, PaymentDetails paymentDetails, IPaymentProcessor paymentProcessor, IReservationService reservationService)
            :base(cart)
        {
            _notificationService = notificationService;
            _paymentDetails = paymentDetails;
            _paymentProcessor = paymentProcessor;
            _reservationService = reservationService;
        }

        public override void Checkout()
        {
            _reservationService.ReserveInventory(_cart.Items);
            _paymentProcessor.ProcessCreditCard(_paymentDetails, _cart.TotalAmount);
            _notificationService.NotifyCustomerOrder(_cart);

            base.Checkout();
        }
    }

    public class PoSCreditCardOrder : OrderBase
    {
        private PaymentDetails _paymentDetails;
        private IPaymentProcessor _paymentProcessor;

        public PoSCreditCardOrder(Cart cart, PaymentDetails paymentDetails, IPaymentProcessor paymentProcessor) : base(cart)
        {
            _paymentDetails = paymentDetails;
            _paymentProcessor = paymentProcessor;
        }

        public override void Checkout()
        {
            _paymentProcessor.ProcessCreditCard(_paymentDetails, _cart.TotalAmount);

            base.Checkout();
        }
    }

    public class PoSCashOrder : OrderBase
    {
        public PoSCashOrder(Cart cart) :base(cart)
        {            
        }
    }      

    //public class Order
    //{
    //    public void Checkout(Cart cart, PaymentDetails paymentDetails, bool notifyCustomer)
    //    {
    //        if (paymentDetails.PaymentMethod == PaymentMethod.CreditCard)
    //        {
    //            ChargeCard(paymentDetails, cart);
    //        }

    //        ReserveInventory(cart);

    //        if(notifyCustomer)
    //        {
    //            NotifyCustomer(cart);
    //        }
    //    }

    //    public void NotifyCustomer(Cart cart)
    //    {
    //        string customerEmail = cart.CustomerEmail;
    //        if (!String.IsNullOrEmpty(customerEmail))
    //        {
    //            using (var message = new MailMessage("orders@somewhere.com", customerEmail))
    //            using (var client = new SmtpClient("localhost"))
    //            {
    //                message.Subject = "Your order placed on " + DateTime.Now.ToString();
    //                message.Body = "Your order details: \n " + cart.ToString();

    //                try
    //                {
    //                    client.Send(message);
    //                }
    //                catch (Exception ex)
    //                {
    //                    Logger.Error("Problem sending notification email", ex);
    //                }
    //            }
    //        }
    //    }

    //    public void ReserveInventory(Cart cart)
    //    {
    //        foreach(var item in cart.Items)
    //        {
    //            try
    //            {
    //                var inventorySystem = new InventorySystem();
    //                inventorySystem.Reserve(item.Sku, item.Quantity);

    //            }
    //            catch (InsufficientInventoryException ex)
    //            {
    //                throw new OrderException("Insufficient inventory for item " + item.Sku, ex);
    //            }
    //            catch (Exception ex)
    //            {
    //                throw new OrderException("Problem reserving inventory", ex);
    //            }
    //        }
    //    }

    //    public void ChargeCard(PaymentDetails paymentDetails, Cart cart)
    //    {
    //        using (var paymentGateway = new PaymentGateway())
    //        {
    //            try
    //            {
    //                paymentGateway.Credentials = "account credentials";
    //                paymentGateway.CardNumber = paymentDetails.CreditCardNumber;
    //                paymentGateway.ExpiresMonth = paymentDetails.ExpiresMonth;
    //                paymentGateway.ExpiresYear = paymentDetails.ExpiresYear;
    //                paymentGateway.NameOnCard = paymentDetails.CardholderName;
    //                paymentGateway.AmountToCharge = cart.TotalAmount;

    //                paymentGateway.Charge();
    //            }
    //            catch (AvsMismatchException ex)
    //            {
    //                throw new OrderException("The card gateway rejected the card based on the address provided.", ex);
    //            }
    //            catch (Exception ex)
    //            {
    //                throw new OrderException("There was a problem with your card.", ex);
    //            }
    //        }
    //    }
    //}
}

    public class OrderException : Exception
    {
        public OrderException(string message, Exception innerException)
            : base(message, innerException)
        {            
        }
    }