﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ActiveRecord.Model;

namespace ActiveRecord.WebUI.MVC.Controllers
{
    public class BlogController : Controller
    {
        //
        // GET: /Blog/

        public ActionResult Index()
        {
            Post[] posts = Post.FindAll();

            if (posts.Count() > 0)
            {
                ViewBag.AllPosts = posts;
                ViewBag.LatestPost = Post.FindLastestPost();

                return View();
            }

            return RedirectToAction("Create");
        }

        [HttpPost]
        public ActionResult CreateComment(int id, FormCollection collection)
        {
            Post post = Post.Find(id);

            Comment comment = new Comment
                {
                    Post = post,
                    Author = collection["Author"],
                    Text = collection["Comment"],
                    DateAdded = DateTime.Now
                };
            
            comment.Save();

            return RedirectToAction("Detail", new { id = post.Id });
        }

        public ActionResult Detail(int id)
        {
            ViewBag.AllPosts = Post.FindAll();
            ViewBag.LatestPost = Post.Find(id);

            return View("Index");
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Post post)
        {
            post.DateAdded = DateTime.Now;
            post.Save();
            return RedirectToAction("Detail", new { id = post.Id });
        }
    }
}
